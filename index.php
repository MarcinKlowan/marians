<?php
declare(strict_types=1); // nowość w PHP 7 # od teraz ZAWSZE stosujemy ten zapis == nasz kontrakt
# Własne funkcje  # zapis camelCase, klamra piętro niżej

/**
 * @int Zwraca .
 * 
 * @param int  
 */




require_once 'Customer.php';

$customer = new Customer();   // new oznacza, że tworzymy nowy obiekt
// $customer->name = 'Marian';  to ukrywamy (naprawiamy)
$customer->setName('Marian');
echo $customer->getDiscount();  // po nawiasach poznajemy że to metoda

echo PHP_EOL;
var_export($customer);


