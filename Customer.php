<?php
declare(strict_types=1); // nowość w PHP 7 # od teraz ZAWSZE stosujemy ten zapis == nasz kontrakt
# Własne funkcje  # zapis camelCase, klamra piętro niżej

/**
 * @int Zwraca .
 * 
 * @param int  
 */


// skąd wiemy? bo wielką literą, bo PSR nam każe
 class Customer   // klasa
 {
    private $name;   // Własność $name   public , bo PSR nam każe

    public function getDiscount(): int
    {
        return 20;
    }

    // czasem jest tak, że własność się ukrywa a settery(metody) ukrywa się, tzw. enkapsukacja * bezpieczeństwo

    public function setName(string $name): void
    {
        $this->name = $name;   // setter - rodzina klas ustawiająca coś * Zob. getter
    }


}
